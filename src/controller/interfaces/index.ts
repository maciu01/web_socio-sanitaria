import { BasicResponse } from "src/controller/types";


export interface IHelloController{
    getMessage(name?:string): Promise<BasicResponse >
}