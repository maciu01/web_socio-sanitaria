/**
 * Basic JSON response for Controllers
 */

export type BasicResponse = {
    message: string
}

/**
 * Error Response for Controllers
 */

export type ErrorRespone = {
    error: string,
    message:string
}
